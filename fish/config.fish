if status is-interactive
    # Commands to run in interactive sessions can go here
    eval (starship init fish)
    fish_add_path $HOME/bin

    alias "zig\ build\ run"='zigup master --path-link ~/.zig/zig 2> /dev/null; zig build run'
    # Plan9 ACME making me type the whole command instead of completing w/ TAB
    alias .="clear"

#    export PLAN9=/usr/lib/plan9
#    fish_add_path $PLAN9/bin

    alias ls="ls --color=auto --file-type -Sch --format single-column"

end
