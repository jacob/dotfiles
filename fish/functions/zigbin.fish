function zigbin
    if test -e $argv
        curl -F'file=@-' https://zigbin.io <$argv
    else
        echo "Please enter a valid filename to upload."
    end
end
